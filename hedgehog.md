Once lived a hedgehog  
Under family home key  
Unlike the fur of a dog  
His armour was hard and spiky  
  
"Why the distance" asked a rabbit  
"You seem nice and friendly  
Play with us and dab it  
We'll get along surely"  
  
"Thanks for the offer, so generous  
Unlike your fur, warm and cozy  
My skin is rather dangerous  
To protect me from likes of coyote  
  
I have acquintances over  
Some of them even friendly  
But when I dared get close  
My spikes hurt them plenty"  
  
One day he saw a rat  
Thought, she is so much like me  
Despite my hard hat  
She might even like me  
  
He moved in to say hi  
And exposed his soft belly  
Still his spikes stung her dry  
She moved away in agony  
  
He wanted to cut his spike array  
But he couldn't fight properly  
He would be an easy prey  
Without his weaponry  
  
So he kept his distance  
Though his heart was achy  
It was a lonely existence  
But ensured others' safety  
